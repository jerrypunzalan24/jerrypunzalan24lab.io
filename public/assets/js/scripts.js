
  $(document).ready(function(){
    $('#right').click(function(){
      $(this).hide()
      $('#page').toggle("slide", {direction:"left"}, 350,function(){
        $('#page2').toggle("slide", {direction:"right"}, 350,function(){
          $('#left').show()
        })
      })
    })
    $('#left').click(function(){
      $(this).hide()
      $('#page2').toggle("slide",{direction:"right"},350,function(){
        $('#page').toggle("slide", {direction:"left"},350,function(){
          $("#right").show()
        });
      })
  

    })
    var preload = new createjs.LoadQueue(true)
    preload.loadFile("assets/img/icon.jpg")
    preload.loadFile("assets/img/twitter.png")
    preload.loadFile("assets/img/resume.png")
    preload.loadFile("assets/img/gitlab.png")
    preload.loadFile("assets/img/git.png")
    preload.loadFile("assets/files/resume.docx")
    preload.loadFile("assets/fonts/ARCADECLASSIC.TTF")
    preload.loadFile("assets/fonts/ka1.ttf")
    preload.loadFile("assets/css/bootstrap.min.css")
    preload.loadFile("assets/js/bootstrap.min.js")
    preload.loadFile("assets/img/video-bg.mp4")
    preload.on("fileload", function(e){
      var count = e.currentTarget._numItemsLoaded, max = e.currentTarget._numItems
      $("#progress").html(parseInt(count/max *100) + "%")
      $("#progress").css("width", parseInt(count/max *100) + "%")
    })
    preload.on("complete", function(e){
      $("#loading").fadeOut("slow", function(){
        $('#page').fadeIn()
        $('#myVideo').css('display','block')
        $('#color-bg').css('display','block')
        // $('body').css('background-size','cover')
      })
    })
 


    $('#recipeCarousel').carousel({
  interval: 1000000
})
$('#recipeCarousel').on('slide.bs.carousel',function(){
  var images = $(this).find('div.carousel-inner.w-100').children()
  for(var x = 0; x < images.length ;x++){
    console.log(images[x].className)
    if(images[x].className.includes('carousel-item active')){
      var firstimage = images[x].getElementsByTagName('img')['3'].attributes['1'].value
      if(firstimage == 'bass'){
        $('#image').attr('src', 'assets/img/BASS.png')
        $('#title').html("Bullying Awareness Through Situational Scenario")
        $('#description').html("Download: <a href ='assets/files/bass.apk'>here</a>")
      }
      else if(firstimage == 'sa'){
        $('#image').attr('src','assets/img/sa.png')
        $('#title').html("Similarity Assessment")
        $("#description").html("Link: <a href ='https://similarity-assessment.herokuapp.com'>https://similarity-assessment.herokuapp.com</a>")
      }
      else if(firstimage == 'pr'){
        $('#image').attr('src', 'assets/img/pr.png')
        $('#title').html("UE CCSS RND Panelist Registration")
        $('#description').html("Link: <a href ='http://uepanelistregistration-env.7gh2qruqgy.us-east-2.elasticbeanstalk.com/'>http://uepanelistregistration-env.7gh2qruqgy.us-east-2.elasticbeanstalk.com/</a> ")
      }
      else if(firstimage == 'beta'){
        $("#image").attr('src', 'assets/img/sachinormal.png')
        $('#title').html("Basic English Tutoring Aide")
        $("#description").html("Link : Coming Soon")
      }
      console.log(images[x].getElementsByTagName('img')['3'].attributes)
    }
  }
})
$('.carousel .carousel-item').each(function(){
    var next = $(this).next();
    
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
  
    for (var i=0;i<3;i++) {
        next=next.next();

        if (!next.length) {
        	next = $(this).siblings(':first');
      	}
        
        next.children(':first-child').clone().appendTo($(this));
      }
});
  })